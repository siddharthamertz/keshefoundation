<?php
if ( ! defined( 'ABSPATH' ) ) {
	die( '-1' );
}
require_once( 'class-vc-grids-common.php' );
$gridParams = VcGridsCommon::getBasicAtts();

return array(
	'name' => __( 'Post Grid', 'js_composer' ),
	'base' => 'vc_basic_grid',
    'class' => 'ct_tt_notstyled_vc',
	'icon' => 'icon-wpb-application-icon-large',
	'category' => __( 'Content', 'js_composer' ),
	'description' => __( 'Not specifically styled', 'js_composer' ),
	'params' => $gridParams,
);
