<?php

require_once( 'class-vc-grids-common.php' );
$mediaGridParams = VcGridsCommon::getMediaCommonAtts();

return array(
	'name' => __( 'Media Grid', 'js_composer' ),
	'base' => 'vc_media_grid',
    'class' => 'ct_tt_notstyled_vc',
	'icon' => 'vc_icon-vc-media-grid',
	'category' => __( 'Content', 'js_composer' ),
	'description' => __( 'Not specifically styled', 'js_composer' ),
	'params' => $mediaGridParams,
);
