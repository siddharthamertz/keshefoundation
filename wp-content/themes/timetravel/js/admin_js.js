/*
		jQuery(function($) {
			jQuery('#media-items').bind('DOMNodeInserted',function(){
				jQuery('input[value="Insert into Post"]').each(function(){
						jQuery(this).attr('value','Use This Image');
				});
			});
			jQuery('.custom_upload_image_button').live("click", function() {
				window.restore_send_to_editor = window.send_to_editor;
				formfield = jQuery(this).siblings('.custom_upload_image');
				preview = jQuery(this).siblings('.custom_preview_image');
				tb_show('', 'media-upload.php?type=image&TB_iframe=true');
				window.send_to_editor = function(html) {
					imgurl = jQuery('img',html).attr('src');
					classes = jQuery('img', html).attr('class');
					id = classes.replace(/(.*?)wp-image-/, '');
					formfield.val(id);
					preview.attr('src', imgurl);
					tb_remove();
					window.send_to_editor = window.restore_send_to_editor;
				}
				return false;
			});
		
			jQuery('.custom_clear_image_button').click(function() {
							
				var defaultImage = jQuery(this).parent().siblings('.custom_default_image').text();
				jQuery(this).parent().siblings('.custom_upload_image').val('');
				jQuery(this).parent().siblings('.custom_preview_image').attr('src', defaultImage);
				return false;
			});
		
			jQuery('.repeatable-add').click(function() {
				field = jQuery(this).closest('td').find('.custom_repeatable li:last').clone(true);
				fieldLocation = jQuery(this).closest('td').find('.custom_repeatable li:last');
				jQuery('input.custom_upload_image', field).val('').attr('name', function(index, name) {
					return name.replace(/(\d+)/, function(fullMatch, n) {
					return Number(n) + 1;
				});

			})
			jQuery('.custom_preview_image', field).attr('src', '');
			field.insertAfter(fieldLocation, jQuery(this).closest('td'))
			return false;
		});
		
		jQuery('.repeatable-remove').click(function(){

			jQuery(this).parent().remove();
			return false;
		});
	});*/
	
/*
jQuery(function($) {
    'use strict';
	jQuery('#media-items').bind('DOMNodeInserted',function(){
		jQuery('input[value="Insert into Post"]').each(function(){
				jQuery(this).attr('value','Use This Image');
		});
	});
	$('.custom_upload_image_button').click(function(e) {
		e.preventDefault();
		var formfield = jQuery(this).siblings('.custom_upload_image');
		var preview = jQuery(this).siblings('.custom_preview_image');

		var custom_uploader = wp.media({
			title: 'Select image',
			button: {
				text: 'Select image'
			},
			multiple: false  // Set this to true to allow multiple files to be selected
		})
		.on('select', function() {
			var attachment = custom_uploader.state().get('selection').first().toJSON();
			formfield.val(attachment.id);
			preview.attr('src', attachment.url);
		})
		.open();
	});
			
	jQuery('.custom_clear_image_button').click(function() {
					
		var defaultImage = jQuery(this).parent().siblings('.custom_default_image').text();
		jQuery(this).parent().siblings('.custom_upload_image').val('');
		jQuery(this).parent().siblings('.custom_preview_image').attr('src', defaultImage);
        
		return false;
	});

	jQuery('.repeatable-add').click(function() {
    
		var field = jQuery(this).closest('td').find('.custom_repeatable li:last').clone(true);
		var fieldLocation = jQuery(this).closest('td').find('.custom_repeatable li:last');
		jQuery('input.custom_upload_image', field).val('').attr('name', function(index, name) {
			return name.replace(/(\d+)/, function(fullMatch, n) {
			return Number(n) + 1;
		});

	})
	jQuery('.custom_preview_image', field).attr('src', '');
		field.insertAfter(fieldLocation, jQuery(this).closest('section'))
		return false;
	});

	jQuery('.repeatable-remove').click(function(){
		if(jQuery('.repeatable-remove').length <2){
			var defaultImage = jQuery(this).siblings('.custom_default_image').text();
			jQuery(this).siblings('.custom_upload_image').val('');
			jQuery(this).siblings('.custom_preview_image').attr('src', defaultImage);
		     
		}else{
			jQuery(this).parent().remove();
		}
		
		return false;
	});
});*/


jQuery(function($) {
    'use strict';
	jQuery('#media-items').bind('DOMNodeInserted',function(){
		jQuery('input[value="Insert into Post"]').each(function(){
				jQuery(this).attr('value','Use This Image');
		});
	});
	$('.custom_upload_image_button').click(function(e) {
		e.preventDefault();
		var formfield = jQuery(this).siblings('.custom_upload_image');
		var preview = jQuery(this).siblings('.custom_preview_image');

		var custom_uploader = wp.media({
			title: 'Select image',
			button: {
				text: 'Select image'
			},
			multiple: false  // Set this to true to allow multiple files to be selected
		})
		.on('select', function() {
			var attachment = custom_uploader.state().get('selection').first().toJSON();
			formfield.val(attachment.id);
			preview.attr('src', attachment.url);
		})
		.open();
	});
			
	jQuery('.custom_clear_image_button').click(function() {
					
		var defaultImage = jQuery(this).parent().siblings('.custom_default_image').text();
		jQuery(this).parent().siblings('.custom_upload_image').val('');
		jQuery(this).parent().siblings('.custom_preview_image').attr('src', defaultImage);
        
		return false;
	});

	jQuery('.repeatable-add').click(function() {
		var field = jQuery(this).closest('td').find('.custom_repeatable li:last').clone(true);
		var fieldLocation = jQuery(this).closest('td').find('.custom_repeatable li:last');
        var defaultImage = jQuery(this).closest('td').find('.custom_default_image:last').text();
		jQuery('input.custom_upload_image', field).val('').attr('name', function(index, name) {
			return name.replace(/(\d+)/, function(fullMatch, n) {
			return Number(n) + 1;
                
		});

	})
         jQuery('.custom_preview_image', field).attr('src',defaultImage);
		field.insertAfter(fieldLocation, jQuery(this).closest('section'))
		return false;
	});

	jQuery('.repeatable-remove').click(function(){
		if(jQuery('.repeatable-remove').length <2){
			var defaultImage = jQuery(this).siblings('.custom_default_image').text();
			jQuery(this).siblings('.custom_upload_image').val('');
			jQuery(this).siblings('.custom_preview_image').attr('src', defaultImage);
		     
		}else{
			jQuery(this).parent().remove();
		}
		
		return false;
	});
});



